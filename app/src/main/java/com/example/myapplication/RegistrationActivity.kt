package com.example.myapplication

import android.net.nsd.NsdManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class RegistrationActivity : AppCompatActivity() {

    private lateinit var EmailAddress: EditText
    private lateinit var Password: EditText
    private lateinit var repeatPassword: EditText
    private lateinit var buttonRegistration: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        init()

        RegistrationListeners()


    }


    private fun init() {
        EmailAddress = findViewById(R.id.EmailAddress)
        Password = findViewById(R.id.Password)
        repeatPassword = findViewById(R.id.repeatPassword)
        buttonRegistration = findViewById(R.id.buttonRegistration)

    }

    private fun RegistrationListeners() {
        buttonRegistration.setOnClickListener {

            val email = EmailAddress.text.toString()
            val password = Password.text.toString()
            val repeatPassword = repeatPassword.toString()

            if (password != repeatPassword) {
                Toast.makeText(this, "Incorrect Password", Toast.LENGTH_SHORT).show()
                return@setOnClickListener

            }
            FirebaseAuth
                .getInstance()
                .createUserWithEmailAndPassword(email, password)



        }
    }
}